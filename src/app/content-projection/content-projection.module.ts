import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentProjectionComponent } from './content-projection.component';
import { RouterModule } from '@angular/router';
import { ProjectionComponent } from './projection/projection.component';

@NgModule({
  declarations: [ContentProjectionComponent, ProjectionComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentProjectionComponent,
      },
    ]),
  ],
})
export class ContentProjectionModule {}
