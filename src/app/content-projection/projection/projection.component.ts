import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-projection',
  templateUrl: './projection.component.html',
  styleUrls: ['./projection.component.scss'],
})
export class ProjectionComponent {
  @Input() title: string = '';
}
