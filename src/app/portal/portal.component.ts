import { CdkPortalOutlet, ComponentPortal, DomPortal, Portal, TemplatePortal } from '@angular/cdk/portal';
import { Component, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ArticleComponent } from '../dynamic-component/article/article.component';
import { articles } from '../dynamic-component/articles';

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.scss'],
})
export class PortalComponent {
  @ViewChild(CdkPortalOutlet) portalOutlet!: CdkPortalOutlet;
  @ViewChild('template') template!: TemplateRef<unknown>;

  detach: boolean = true;

  constructor(private _viewContainerRef: ViewContainerRef) {}

  renderComponentPortal(): void {
    this._detachAll();

    const componentPortal = new ComponentPortal(ArticleComponent);
    const componentRef = this.portalOutlet.attachComponentPortal(componentPortal);
    componentRef.instance.title = articles[0].id.toString();
    componentRef.instance.content = articles[0].article;
  }

  renderTemplatePortal(): void {
    this._detachAll();

    const templatePortal = new TemplatePortal(this.template, this._viewContainerRef);
    this.portalOutlet.attachTemplatePortal(templatePortal);
  }

  renderDomPortal(): void {
    this._detachAll();

    const p = document.createElement('p');
    document.documentElement.appendChild(p);
    p.innerText = 'DOM Portal content';

    const domPortal = new DomPortal(p);
    this.portalOutlet.attach(domPortal);
  }

  private _detachAll(): void {
    while (this.detach && this.portalOutlet.hasAttached()) {
      this.detach && this.portalOutlet.detach();
    }
  }
}
