import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalComponent } from './portal.component';
import { RouterModule } from '@angular/router';
import { PortalModule as CdkPortalModule } from '@angular/cdk/portal';
import { ArticleModule } from '../dynamic-component/article/article.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [PortalComponent],
  imports: [
    CommonModule,
    CdkPortalModule,
    ArticleModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: PortalComponent,
      },
    ]),
  ],
})
export class PortalModule {}
