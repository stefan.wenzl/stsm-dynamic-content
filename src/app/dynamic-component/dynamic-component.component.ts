import { Component, ComponentRef, QueryList, TemplateRef, ViewChild, ViewChildren, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ArticleComponent } from './article/article.component';
import { articles } from './articles';

@Component({
  selector: 'app-dynamic-component',
  templateUrl: './dynamic-component.component.html',
  styleUrls: ['./dynamic-component.component.scss'],
})
export class DynamicComponentComponent {
  @ViewChild('main', { read: ViewContainerRef }) mainRef!: ViewContainerRef;
  @ViewChild('template', { read: TemplateRef }) templateRef!: TemplateRef<unknown>;

  indexControl: FormControl = new FormControl(0);

  private _articleCounter: number = 0;
  private _articleRefs: ComponentRef<ArticleComponent>[] = [];

  private get _index(): number {
    return this.indexControl.value;
  }

  addArticle(): void {
    const componentRef = this.mainRef.createComponent(ArticleComponent, {
      index: this._index,
    });
    componentRef.instance.title = articles[this._articleCounter].id.toString();
    componentRef.instance.content = articles[this._articleCounter].article;
    this._articleRefs.splice(this._index, 0, componentRef);
    this._articleCounter++;
  }

  removeArticle(): void {
    this.mainRef.remove(this._index);
    this._articleRefs.splice(this._index, 1);
  }

  addTemplateContent(): void {
    const articleRef = this._articleRefs[this._index];
    articleRef.instance.template = this.templateRef;
  }
}
