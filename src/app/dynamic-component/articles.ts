export interface Article {
  id: number;
  article: string;
}

export const articles: Article[] = [
  {
    id: 0,
    article:
      'Ex et duis mollit excepteur dolor eiusmod esse consequat. Ex et amet eiusmod aute occaecat ea eiusmod sit mollit. Incididunt consectetur ut est laboris Lorem. Elit occaecat elit non qui fugiat. Exercitation aliqua pariatur et consequat nulla sit veniam.',
  },
  {
    id: 10,
    article:
      'Tempor in occaecat ex cupidatat do eiusmod voluptate nostrud. Incididunt Lorem nulla aute consectetur. Quis est nostrud dolor est culpa consectetur et culpa exercitation irure aliqua. Sunt laboris Lorem adipisicing ex dolore labore ipsum laboris qui esse et non. Reprehenderit tempor irure commodo eu enim do ullamco dolore.',
  },
  {
    id: 20,
    article:
      'Quis ut dolore id consectetur cillum. Nisi laboris tempor pariatur elit duis amet laborum deserunt tempor. Dolor non amet deserunt mollit do cupidatat. Cupidatat ad sint qui ex dolore proident aliqua deserunt esse minim exercitation do est enim. Irure quis excepteur enim ea enim.',
  },
  {
    id: 30,
    article:
      'Adipisicing eu laborum voluptate fugiat non incididunt ex eiusmod. In dolor elit duis velit ut. Irure ut excepteur ad commodo do sint. Amet consectetur occaecat sunt esse sunt anim cillum labore ipsum minim. Ipsum exercitation ipsum ex dolore cupidatat mollit mollit adipisicing irure proident mollit incididunt qui incididunt.',
  },
  {
    id: 40,
    article:
      'Tempor magna ut irure adipisicing ea commodo id irure esse sunt. Ex veniam nostrud ex do labore occaecat sint minim esse. Ut irure laborum duis eiusmod nulla anim est nostrud. Qui qui deserunt ex do ullamco proident nostrud sint consectetur laborum ut pariatur qui nulla. Ut pariatur excepteur minim culpa consectetur commodo incididunt.',
  },
  {
    id: 50,
    article:
      'Qui commodo esse nisi voluptate elit Lorem consequat eu labore. Esse eu id sint nostrud qui sunt. Culpa incididunt eiusmod voluptate pariatur anim duis et exercitation est tempor est ullamco. Velit irure fugiat ullamco Lorem velit consequat tempor ea. Esse quis magna nostrud tempor sunt exercitation elit.',
  },
  {
    id: 60,
    article:
      'Sint quis cillum nulla consequat dolor nulla. Nostrud do nulla labore in occaecat laborum quis fugiat mollit excepteur eu. Ut duis reprehenderit commodo est nisi pariatur velit tempor. Aliqua laboris id nisi nulla proident labore. Occaecat irure consectetur minim incididunt dolor consectetur consectetur qui eiusmod ut laboris nisi labore.',
  },
  {
    id: 70,
    article:
      'Qui laborum ut id deserunt. Enim voluptate eu sunt velit aliqua cupidatat laboris nulla sunt adipisicing ea. Irure consequat eiusmod occaecat exercitation officia aliquip nulla elit commodo eu do id consequat. Consequat aliquip adipisicing sit mollit cupidatat laboris velit. Minim occaecat tempor anim aute veniam cillum commodo anim in.',
  },
  {
    id: 80,
    article:
      'Pariatur minim ex occaecat cillum. Eu elit deserunt magna nostrud aliquip pariatur dolor nulla culpa excepteur. Ut commodo nulla anim nostrud consequat adipisicing ut consequat esse proident proident pariatur eu. Minim do fugiat sit magna amet Lorem enim est excepteur. Velit excepteur aliquip ullamco sint veniam eu aliquip id.',
  },
  {
    id: 90,
    article:
      'Ullamco sunt mollit eiusmod sit magna veniam velit deserunt consequat ea do. Culpa irure et ea reprehenderit. Ea esse officia ut laborum nulla esse. Laboris eiusmod deserunt eu ullamco. Anim dolore exercitation consectetur labore cupidatat id culpa ipsum aliqua consectetur magna cupidatat.',
  },
  {
    id: 100,
    article:
      'Deserunt exercitation veniam enim non. Officia eiusmod et non sunt ad duis in elit sunt exercitation. Officia adipisicing elit id quis tempor voluptate duis ut fugiat. Irure excepteur reprehenderit voluptate incididunt sint aliquip eu minim veniam labore adipisicing. Ex officia nostrud duis veniam ea.',
  },
  {
    id: 110,
    article:
      'Ex sunt duis eiusmod exercitation est magna pariatur ex nisi cillum labore nulla ut quis. Esse excepteur qui ut eu minim dolor nulla reprehenderit ut id et reprehenderit sint. Non qui velit fugiat est ut reprehenderit sunt laboris nisi ipsum voluptate. In tempor mollit excepteur id sint pariatur sint pariatur exercitation quis irure do esse. Deserunt officia laborum sunt fugiat commodo cupidatat nisi ea.',
  },
  {
    id: 120,
    article:
      'Non exercitation labore labore nulla laboris duis do qui ex tempor adipisicing. Elit sint mollit labore sit minim mollit adipisicing. Ipsum labore proident voluptate ipsum laborum elit ea. Tempor eiusmod officia laboris consequat consequat non do sit Lorem. Ut duis ea magna esse.',
  },
  {
    id: 130,
    article:
      'Fugiat amet commodo mollit nostrud. Eiusmod fugiat irure culpa aliquip laboris magna minim irure est. Pariatur id occaecat labore occaecat sint excepteur velit magna sint tempor duis. Veniam elit adipisicing anim voluptate nulla qui Lorem sint commodo duis. Reprehenderit quis tempor eiusmod fugiat consequat ut mollit eiusmod fugiat deserunt do est cillum.',
  },
  {
    id: 140,
    article:
      'Id tempor incididunt cillum minim nulla enim ad. In officia aliqua dolore labore enim. Ex consequat velit eu tempor amet. Voluptate ipsum ex aliquip et anim veniam dolor et esse aliqua. Eiusmod excepteur id quis duis est ipsum sit adipisicing.',
  },
  {
    id: 150,
    article:
      'Do commodo amet voluptate eu nulla officia magna. Dolore velit do do nostrud officia ex consectetur Lorem velit. Ex aute eiusmod dolore irure anim sunt ea excepteur quis laboris officia. Cillum occaecat et ullamco adipisicing quis duis. Ipsum irure qui mollit ullamco nisi aliquip.',
  },
  {
    id: 160,
    article:
      'Deserunt proident ex adipisicing nisi. Irure eu occaecat laboris nisi do sunt nulla laborum excepteur aliquip eu cupidatat amet. Aute ipsum laborum est sit exercitation tempor ad amet minim. Eu fugiat mollit officia irure. Nisi magna ut deserunt fugiat exercitation veniam sit irure ullamco sint duis.',
  },
  {
    id: 170,
    article:
      'Veniam et magna sit aliqua tempor veniam duis. Sint non sint pariatur anim incididunt ea culpa. Velit irure velit consectetur aliquip id labore culpa Lorem amet ut aliquip deserunt aliquip aute. Commodo dolor officia cupidatat exercitation eu. Dolore duis ut nostrud deserunt in amet id consectetur enim Lorem amet ullamco officia.',
  },
  {
    id: 180,
    article:
      'Dolore ipsum proident adipisicing mollit veniam in dolor aliqua. Fugiat excepteur sunt reprehenderit est aliquip velit labore sint duis cupidatat ipsum. Elit excepteur quis minim veniam. Ipsum fugiat ipsum exercitation non nostrud culpa. Nostrud est aliqua proident voluptate aute Lorem tempor reprehenderit voluptate sunt ipsum.',
  },
  {
    id: 190,
    article:
      'Amet esse culpa tempor cupidatat nulla culpa amet esse. Elit pariatur non anim id culpa cupidatat proident cupidatat dolore dolore incididunt tempor officia aliqua. Pariatur laboris esse esse in exercitation excepteur laborum incididunt magna adipisicing deserunt aliqua consequat. Non cupidatat cupidatat nostrud eu magna duis ea ullamco magna. Exercitation nisi laboris dolore esse qui esse laboris sunt deserunt.',
  },
];
