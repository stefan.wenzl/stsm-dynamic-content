import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicComponentComponent } from './dynamic-component.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ArticleModule } from './article/article.module';

@NgModule({
  declarations: [DynamicComponentComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ArticleModule,
    RouterModule.forChild([
      {
        path: '',
        component: DynamicComponentComponent,
      },
    ]),
  ],
})
export class DynamicComponentModule {}
