import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent {
  @Input() content!: string;
  @Input() title!: string;
  @Input() template?: TemplateRef<unknown>;
}
