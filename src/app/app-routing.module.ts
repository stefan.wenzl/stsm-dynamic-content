import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'content-projection',
    title: 'Content Projection',
    loadChildren: () => import('./content-projection/content-projection.module').then((m) => m.ContentProjectionModule),
  },
  {
    path: 'template-outlet',
    title: 'Template Outlet',
    loadChildren: () => import('./template-outlet/template-outlet.module').then((m) => m.TemplateOutletModule),
  },
  {
    path: 'dynamic-component',
    title: 'Dynamic Component',
    loadChildren: () => import('./dynamic-component/dynamic-component.module').then((m) => m.DynamicComponentModule),
  },
  {
    path: 'portal',
    title: 'Portal',
    loadChildren: () => import('./portal/portal.module').then((m) => m.PortalModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
