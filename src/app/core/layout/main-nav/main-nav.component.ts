import { Component } from '@angular/core';
import { routes } from 'src/app/app-routing.module';

@Component({
  selector: 'nav[appMainNav]',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss'],
})
export class MainNavComponent {
  routes = routes;
}
