import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateOutletComponent } from './template-outlet.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [TemplateOutletComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: TemplateOutletComponent,
      },
    ]),
  ],
})
export class TemplateOutletModule {}
